<?php

namespace Smtm\Psr\Container\Factory;

use Psr\Container\ContainerInterface;

interface AbstractFactoryInterface extends FactoryInterface
{
    /**
     * Can the factory create an instance for the service?
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName);
}
