<?php

declare(strict_types=1);

namespace Smtm\Psr\Container\Exception;

use Psr\Container\ContainerExceptionInterface;
use RuntimeException;

class MissingDependencyException extends RuntimeException implements
    ContainerExceptionInterface
{
}
