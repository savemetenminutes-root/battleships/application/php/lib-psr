<?php

namespace Smtm\Psr\Http\Message\Request\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Psr\Container\Factory\FactoryInterface;
use Smtm\Psr\Http\Message\Request\CurrentRequestDecorator;
use Smtm\Psr\Http\Message\Stream\CurrentRequestBodyStreamDecorator;
use Smtm\Psr\Http\Message\Uri\CurrentUriDecorator;

class CurrentRequestDecoratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $uri    = $container->get(CurrentUriDecorator::class);
        $stream = $container->get(CurrentRequestBodyStreamDecorator::class);

        $request = new CurrentRequestDecorator();
        $request
            ->withMethod(strtolower($_SERVER['REQUEST_METHOD']))
            ->withUri($uri)
            ->withBody($stream);

        return $request;
    }
}
