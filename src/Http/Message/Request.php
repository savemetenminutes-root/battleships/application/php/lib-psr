<?php

namespace Smtm\Psr\Http\Message;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

class Request extends Message implements RequestInterface
{
    protected $method;
    protected $requestTarget;
    protected $uri;

    /**
     * @return string|void
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return RequestInterface|void
     */
    public function withMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string|void
     */
    public function getRequestTarget()
    {
        return $this->requestTarget;
    }

    /**
     * @param mixed $requestTarget
     * @return RequestInterface|void
     */
    public function withRequestTarget($requestTarget)
    {
        $this->requestTarget = $requestTarget;
        return $this;
    }

    /**
     * @return UriInterface|void
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param UriInterface $uri
     * @param bool $preserveHost
     * @return RequestInterface|void
     */
    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        $this->uri = $uri;
        return $this;
    }
}
