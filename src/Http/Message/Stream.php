<?php

namespace Smtm\Psr\Http\Message;

use Psr\Http\Message\StreamInterface;

class Stream implements StreamInterface
{
    protected $resource;

    public function __construct(string $file, $mode = 'r')
    {
        $this->resource = fopen($file, $mode);
    }

    /**
     * @return bool|string
     */
    public function __toString()
    {
        return stream_get_contents($this->resource);
    }

    /**
     * @return bool
     */
    public function close(): bool
    {
        return fclose($this->resource);
    }

    /**
     * @return resource|void|null
     */
    public function detach()
    {
        // TODO: Implement detach() method.
    }

    /**
     * @return bool
     */
    public function eof(): bool
    {
        return fseek($this->resource, 0, SEEK_END);
    }

    /**
     * @return string|void
     */
    public function getContents()
    {
        return stream_get_contents($this->resource);
    }

    /**
     * @param null $key
     * @return array|mixed|void|null
     */
    public function getMetadata($key = null)
    {
        return stream_get_meta_data($this->resource);
    }

    /**
     * @return int|void|null
     */
    public function getSize()
    {
        return strlen(stream_get_contents($this->resource));
    }

    /**
     * @return bool|void
     */
    public function isReadable()
    {
    }

    /**
     * @return bool|void
     */
    public function isSeekable()
    {
        // TODO: Implement isSeekable() method.
    }

    /**
     * @return bool|void
     */
    public function isWritable()
    {
        // TODO: Implement isWritable() method.
    }

    /**
     * @param int $length
     * @return string|bool
     */
    public function read($length)
    {
        return fread($this->resource, $length);
    }

    /**
     * @return bool
     */
    public function rewind(): bool
    {
        return fseek($this->resource, 0, SEEK_SET);
    }

    /**
     * @param int $offset
     * @param int $whence
     * @return bool
     */
    public function seek($offset, $whence = SEEK_SET): bool
    {
        return fseek($this->resource, 0, SEEK_SET);
    }

    /**
     * @return int|void
     */
    public function tell()
    {
        // TODO: Implement tell() method.
    }

    /**
     * @param string $string
     * @return int|bool
     */
    public function write($string)
    {
        return fwrite($this->resource, $string);
    }
}
