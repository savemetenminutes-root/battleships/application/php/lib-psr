<?php

namespace Smtm\Psr\Http\Message\Uri\Parser;

use Psr\Http\Message\UriInterface;
use Smtm\Psr\Http\Message\Uri\ReparsedUri;

class ExtendedParser implements ParserInterface
{
    /*
    const IP_ADDRESS_OCTET_REGEX =
        '(?:(?:[1-2]{1}[0-5]{1}[0-5]{1})|(?:[0]{0,1}[0-9]{1}[0-9]{1})|(?:[0]{0,1}[0]{0,1}[0-9]{1}))';
    const IP_ADDRESS_REGEX =
        self::IP_ADDRESS_OCTET_REGEX .
        '\.' .
        self::IP_ADDRESS_OCTET_REGEX .
        '\.' .
        self::IP_ADDRESS_OCTET_REGEX .
        '\.' .
        self::IP_ADDRESS_OCTET_REGEX;
    const DEFAULT_SCHEME = 'https';
    */
    const ALLOW_ALL = '*';

    /**
     * @var ValidatorInterface
     */
    protected $hostValidator;
    /**
     * @var ValidatorInterface
     */
    protected $ipValidator;
    protected $uriValidator; // TODO: Add custom validator
    protected $allowedSchemes    = [self::ALLOW_ALL];
    protected $disallowedSchemes = [];

    public function parse($uri): UriInterface
    {
        $reparsedUri = new ReparsedUri();

        if (stripos($uri, 'javascript:') === 0) {
            $reparsedUri->setIsJavascript(true);
            $reparsedUri->withScheme('javascript');
            return $reparsedUri;
        } else {
            if (strpos($uri, '#') === 0) {
                $reparsedUri->setIsFragment(true);
                return $reparsedUri;
            }
        }

        $schemeDelimiter = '://';
        if (strpos($uri, '//') === 0) {
            $schemeDelimiter = '//';
        }
        $schemeUri        = explode($schemeDelimiter, $uri);
        $isSchemeRelative = ((count($schemeUri) > 1) && (reset($schemeUri) === '')) ? true : false;
        $noSchemeUri      = array_pop($schemeUri);
        $scheme           = $isSchemeRelative ? null : array_shift($schemeUri);

        $userInfoUri   = explode('@', $noSchemeUri);
        $noUserInfoUri = array_pop($userInfoUri);
        $userInfo      = array_shift($userInfoUri);

        $hostUri         = preg_split('#[\?/\#]#', $noUserInfoUri);
        $hostPort        = array_shift($hostUri);
        $hostPortUri     = explode(':', $hostPort);
        $port            = (count($hostPortUri) > 1) ? array_pop($hostPortUri) : null;
        $port            = ($port !== '') ? $port : null;
        $host            = implode($hostPortUri);
        $validHostPort   = ($this->hostValidator !== null) ? $this->hostValidator->isValid($hostPort) : true;
        $validHost       = ($this->hostValidator !== null) ? $this->hostValidator->isValid($host) : true;
        $validHost       = ($host === 'localhost') ? true : $validHost;
        $host            = ($validHost || ($port !== null)) ? $host : null;
        $isHostIpAddress = ($this->ipValidator !== null) ? $this->ipValidator->isValid($host) : null;
        $subdomains      = null;
        $domain          = null;
        $tld             = null;
        if (!$isHostIpAddress) {
            $domains    = explode('.', $host);
            $tld        = array_pop($domains);
            $domain     = array_pop($domains);
            $subdomains = implode('.', $domains);
        }

        $pathQueryFragmentUri = ($host !== null) ? substr($noUserInfoUri, strlen($hostPort)) : $noUserInfoUri;
        $fragmentUri          = explode('#', $pathQueryFragmentUri);
        $fragment             = (count($fragmentUri) > 1) ? array_pop($fragmentUri) : null;

        $pathQueryUri = implode($fragmentUri);
        $queryUri     = explode('?', $pathQueryUri);
        $query        = (count($queryUri) > 1) ? array_pop($queryUri) : null;

        $isPathRelative = (strpos($pathQueryUri, '/') !== 0) ? true : false;
        $path           = implode($queryUri);
        $path           = ($path !== '') ? $path : null;

        $reparsedUri = $reparsedUri->withScheme($scheme);
        $reparsedUri = $reparsedUri->setSchemeDelimiter($schemeDelimiter);
        $reparsedUri = $reparsedUri->withUserInfo($userInfo);
        $reparsedUri = $reparsedUri->withHost($host);
        $reparsedUri = $reparsedUri->withPort($port);
        $reparsedUri = $reparsedUri->withPath($path);
        $reparsedUri = $reparsedUri->withQuery($query);
        $reparsedUri = $reparsedUri->withFragment($fragment);

        $reparsedUri = $reparsedUri->setTld($tld);
        $reparsedUri = $reparsedUri->setDomain($domain);
        $reparsedUri = $reparsedUri->setSubdomains($subdomains);

        $reparsedUri = $reparsedUri->setIsSchemeRelative($isSchemeRelative);
        $reparsedUri = $reparsedUri->setIsPathRelative($isPathRelative);
        $reparsedUri = $reparsedUri->setIsHostIpAddress($isHostIpAddress);

        return $reparsedUri;
    }

    /**
     * @return mixed
     */
    public function getHostValidator()
    {
        return $this->hostValidator;
    }

    /**
     * @param ValidatorInterface | null $hostValidator
     * @return ExtendedParser
     */
    public function setHostValidator($hostValidator = null): ExtendedParser
    {
        $this->hostValidator = $hostValidator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIpValidator()
    {
        return $this->ipValidator;
    }

    /**
     * @param ValidatorInterface | null $ipValidator
     * @return ExtendedParser
     */
    public function setIpValidator(ValidatorInterface $ipValidator = null): ExtendedParser
    {
        $this->ipValidator = $ipValidator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUriValidator()
    {
        return $this->uriValidator;
    }

    /**
     * @param mixed $uriValidator
     * @return ExtendedParser
     */
    public function setUriValidator($uriValidator): ExtendedParser
    {
        $this->uriValidator = $uriValidator;
        return $this;
    }

    /**
     * @return array
     */
    public function getAllowedSchemes(): array
    {
        return $this->allowedSchemes;
    }

    /**
     * @param array $allowedSchemes
     * @return ExtendedParser
     */
    public function setAllowedSchemes(array $allowedSchemes): ExtendedParser
    {
        $this->allowedSchemes = $allowedSchemes;
        return $this;
    }

    /**
     * @return array
     */
    public function getDisallowedSchemes(): array
    {
        return $this->disallowedSchemes;
    }

    /**
     * @param array $disallowedSchemes
     * @return ExtendedParser
     */
    public function setDisallowedSchemes(array $disallowedSchemes): ExtendedParser
    {
        $this->disallowedSchemes = $disallowedSchemes;
        return $this;
    }
}
