<?php

namespace Smtm\Psr\Http\Message;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

class Message implements MessageInterface
{
    protected $body;
    protected $headers;
    protected $protocolVersion;

    /**
     * @return StreamInterface|void
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param StreamInterface $body
     * @return MessageInterface|void
     */
    public function withBody(StreamInterface $body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $name
     * @return bool|void
     */
    public function hasHeader($name)
    {
        // TODO: Implement hasHeader() method.
    }

    /**
     * @param string $name
     * @return string[]|void
     */
    public function getHeader($name)
    {
        // TODO: Implement getHeader() method.
    }

    /**
     * @param string $name
     * @param string|string[] $value
     * @return MessageInterface|void
     */
    public function withAddedHeader($name, $value)
    {
        // TODO: Implement withAddedHeader() method.
    }

    /**
     * @param string $name
     * @param string|string[] $value
     * @return MessageInterface|void
     */
    public function withHeader($name, $value)
    {
        // TODO: Implement withHeader() method.
    }

    /**
     * @param string $name
     * @return MessageInterface|void
     */
    public function withoutHeader($name)
    {
        // TODO: Implement withoutHeader() method.
    }

    /**
     * @param string $name
     * @return string|void
     */
    public function getHeaderLine($name)
    {
        // TODO: Implement getHeaderLine() method.
    }

    /**
     * @return \string[][]|void
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string|void
     */
    public function getProtocolVersion()
    {
        return $this->protocolVersion;
    }

    /**
     * @param string $version
     * @return MessageInterface|void
     */
    public function withProtocolVersion($version)
    {
        $this->protocolVersion = $version;
        return $this;
    }
}
