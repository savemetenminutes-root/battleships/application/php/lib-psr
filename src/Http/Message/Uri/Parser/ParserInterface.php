<?php

namespace Smtm\Psr\Http\Message\Uri\Parser;

use Psr\Http\Message\UriInterface;

interface ParserInterface
{
    public function parse($uri): UriInterface;
}
