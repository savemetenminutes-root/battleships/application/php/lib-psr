<?php

namespace Smtm\Psr\Http\Message;

use Psr\Http\Message\UriInterface;

class Uri implements UriInterface
{
    protected $authority;
    protected $fragment;
    protected $host;
    protected $path;
    protected $port;
    protected $query;
    protected $scheme;
    protected $userInfo;

    public function __toString()
    {
        // TODO: Implement __toString() method.
    }

    public function getAuthority()
    {
        return $this->authority;
    }

    public function getFragment()
    {
        return $this->fragment;
    }

    public function withFragment($fragment)
    {
        $this->fragment = $fragment;
        return $this;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function withHost($host)
    {
        $this->host = $host;
        return $this;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function withPath($path)
    {
        $this->path = $path;
        return $this;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function withPort($port)
    {
        $this->port = $port;
        return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function withQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function getScheme()
    {
        return $this->scheme;
    }

    public function withScheme($scheme)
    {
        $this->scheme = $scheme;
        return $this;
    }

    public function getUserInfo()
    {
        return $this->userInfo;
    }

    public function withUserInfo($user, $password = null)
    {
        $this->userInfo = $user . ($password !== null ? ':' . $password : '');
        return $this;
    }
}
