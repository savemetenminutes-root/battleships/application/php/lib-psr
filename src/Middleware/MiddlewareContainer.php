<?php

declare(strict_types=1);

namespace Smtm\Frameless\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use function class_exists;

class MiddlewareContainer implements ContainerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns middleware pulled from container, or directly instantiated if
     * not managed by the container.
     *
     * @param string $service
     * @return MiddlewareInterface
     * @throws Exception\MissingDependencyException if the service does not
     *     exist, or is not a valid class name.
     * @throws Exception\InvalidMiddlewareException if the service is not
     *     an instance of MiddlewareInterface.
     */
    public function get($service): MiddlewareInterface
    {
        if (!$this->has($service)) {
            throw Exception\MissingDependencyException::forMiddlewareService($service);
        }

        $middleware = $this->container->has($service)
            ? $this->container->get($service)
            : new $service();

        if (!$middleware instanceof MiddlewareInterface) {
            throw Exception\InvalidMiddlewareException::forMiddlewareService($service, $middleware);
        }

        return $middleware;
    }

    /**
     * Returns true if the service is in the container, or resolves to an
     * autoloadable class name.
     *
     * @param string $service
     * @return bool
     */
    public function has($service): bool
    {
        if ($this->container->has($service)) {
            return true;
        }

        return class_exists($service);
    }
}
