<?php

declare(strict_types=1);

namespace Smtm\Psr\Http;

use Smtm\Frameless\Middleware\Container\MiddlewareContainerFactory;
use Smtm\Frameless\Middleware\Container\MiddlewareFactoryFactory;
use Smtm\Psr\Http\Message\Request\CurrentUriDecorator;
use Smtm\Psr\Http\Message\Request\Factory\CurrentUriDecoratorFactory;

/**
 * The configuration provider for the module
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'shared' => [
                'factories' => [
                ],
            ],
        ];
    }
}
