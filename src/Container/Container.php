<?php

namespace Smtm\Psr\Container;

use Psr\Container\ContainerInterface;
use Smtm\Psr\Container\Exception\MissingDependencyException;
use Smtm\Psr\Container\Exception\MissingDependencyFactoryException;
use Zend\ServiceManager\Factory\InvokableFactory;

class Container implements ContainerInterface, \Interop\Container\ContainerInterface
{
    /**
     * @var array
     */
    protected $dependencies = [];
    /**
     * @var array
     */
    protected $abstractFactories = [];
    /**
     * @var array
     */
    protected $factories = [];
    /**
     * @var array
     */
    protected $instances = [];
    protected $creationContext;

    /**
     * Container constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $sharedServicesInvokables = $config['dependencies']['shared']['invokables'] ?? [];
        foreach ($sharedServicesInvokables as $service) {
            $this->dependencies[$service] = new ServiceBlueprint(InvokableFactory::class);
        }

        $sharedServicesFactories = $config['dependencies']['shared']['factories'] ?? [];
        foreach ($sharedServicesFactories as $service => $factory) {
            $this->dependencies[$service] = new ServiceBlueprint($factory);
        }

        $unsharedServicesInvokables = $config['dependencies']['unshared']['invokables'] ?? [];
        foreach ($unsharedServicesInvokables as $service) {
            $this->dependencies[$service] = new ServiceBlueprint(InvokableFactory::class, false);
        }

        $unsharedServicesFactories = $config['dependencies']['unshared']['factories'] ?? [];
        foreach ($unsharedServicesFactories as $service => $factory) {
            $this->dependencies[$service] = new ServiceBlueprint($factory, false);
        }
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new MissingDependencyException('The service (' . $id . ') has not been registered.');
        }

        return
            !array_key_exists($id, $this->instances)
            || !$this->dependencies[$id]->isShared()
                ? $this->instantiate($id)
                : $this->instances[$id];
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        return array_key_exists($id, $this->dependencies);
    }

    /**
     * @param $id
     * @return object
     * @throws MissingDependencyException
     */
    protected function instantiate($id)
    {
        $service = $this->getFactory($id)($this, $id, []);

        return $service;
    }

    /**
     * Get a factory for the given service name
     *
     * @param string $id
     * @return callable
     * @throws MissingDependencyFactoryException
     */
    protected function getFactory($id)
    {
        $factory = isset($this->factories[$id]) ? $this->factories[$id] : $this->dependencies[$id]->getFactory();

        $lazyLoaded = false;
        if (is_string($factory) && class_exists($factory)) {
            $factory    = new $factory();
            $lazyLoaded = true;
        }

        if (is_callable($factory)) {
            if ($lazyLoaded) {
                $this->factories[$id] = $factory;
            }
            // PHP 5.6 fails on 'class::method' callables unless we explode them:
            if (PHP_MAJOR_VERSION < 7
                && is_string($factory) && strpos($factory, '::') !== false
            ) {
                $factory = explode('::', $factory);
            }
            return $factory;
        }

        // Check abstract factories
        foreach ($this->abstractFactories as $abstractFactory) {
            if ($abstractFactory->canCreate($this->creationContext, $id)) {
                return $abstractFactory;
            }
        }

        throw new MissingDependencyFactoryException(sprintf(
            'Unable to resolve service "%s" to a factory; are you certain you provided it during configuration?',
            $id
        ));
    }
}
