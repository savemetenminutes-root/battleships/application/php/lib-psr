<?php

namespace Smtm\Psr\Http\Message\Uri\Parser\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Psr\Container\Factory\FactoryInterface;
use Smtm\Psr\Http\Message\Uri\Parser\ExtendedParser;

class ExtendedParserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $parser = new ExtendedParser();
        return $parser;
    }
}
