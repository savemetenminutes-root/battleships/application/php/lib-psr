<?php

namespace Smtm\Frameless\Middleware;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class MiddlewareFactory
{
    /**
     * @var MiddlewareContainer
     */
    private $container;

    public function __construct(MiddlewareContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @return MiddlewareInterface
     * @throws Exception\InvalidMiddlewareException if argument is not one of
     *    the specified types.
     */
    public function prepare($middleware): MiddlewareInterface
    {
        if (!$middleware instanceof MiddlewareInterface) {
            throw Exception\InvalidMiddlewareException::forMiddleware($middleware);
        }

        return $middleware;
    }
}
