<?php

declare(strict_types=1);

namespace Smtm\Psr\Middleware;

use Smtm\Psr\Http\Message\Request\CurrentRequestDecorator;
use Smtm\Psr\Http\Message\Request\Factory\CurrentRequestDecoratorFactory;
use Smtm\Psr\Http\Message\Stream\CurrentRequestBodyStreamDecorator;
use Smtm\Psr\Http\Message\Stream\Factory\CurrentRequestBodyStreamDecoratorFactory;
use Smtm\Psr\Http\Message\Uri\CurrentUriDecorator;
use Smtm\Psr\Http\Message\Uri\Factory\CurrentUriDecoratorFactory;
use Smtm\Psr\Http\Message\Uri\Parser\ExtendedParser;
use Smtm\Psr\Http\Message\Uri\Parser\Factory\ExtendedParserFactory;

/**
 * The configuration provider for the module
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'shared' => [
                'factories' => [
                    CurrentUriDecorator::class               => CurrentUriDecoratorFactory::class,
                    CurrentRequestBodyStreamDecorator::class => CurrentRequestBodyStreamDecoratorFactory::class,
                    CurrentRequestDecorator::class           => CurrentRequestDecoratorFactory::class,
                    ExtendedParser::class                    => ExtendedParserFactory::class,
                ],
            ]
        ];
    }
}
