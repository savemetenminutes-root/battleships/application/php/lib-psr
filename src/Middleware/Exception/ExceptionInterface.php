<?php

declare(strict_types=1);

namespace Smtm\Frameless\Middleware\Exception;

/**
 * Marker interface for package-specific exceptions.
 */
interface ExceptionInterface
{
}
