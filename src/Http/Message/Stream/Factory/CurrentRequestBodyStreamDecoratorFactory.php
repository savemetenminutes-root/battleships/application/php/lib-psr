<?php

namespace Smtm\Psr\Http\Message\Stream\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Psr\Container\Factory\FactoryInterface;
use Smtm\Psr\Http\Message\Stream\CurrentRequestBodyStreamDecorator;

class CurrentRequestBodyStreamDecoratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $stream = new CurrentRequestBodyStreamDecorator('php://input', 'r');
        return $stream;
    }
}
