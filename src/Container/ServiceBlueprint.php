<?php

namespace Smtm\Psr\Container;

class ServiceBlueprint
{
    /**
     * @var string|callable
     */
    protected $factory;
    /**
     * @var bool
     */
    protected $shared = true;

    /**
     * ServiceBlueprint constructor.
     * @param $factory
     * @param bool $shared
     */
    public function __construct($factory, bool $shared = true)
    {
        $this->factory = $factory;
        $this->shared  = $shared;
    }

    /**
     * @return callable|string
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return bool
     */
    public function isShared(): bool
    {
        return $this->shared;
    }

    /**
     * @param bool $shared
     * @return ServiceBlueprint
     */
    public function setShared(bool $shared): ServiceBlueprint
    {
        $this->shared = $shared;
        return $this;
    }
}
