<?php

namespace Smtm\Psr\Http\Message\Uri\Parser;

use Smtm\Psr\Http\Message\Uri;

class ReparsedUri extends Uri
{
    protected $isJavascript;
    protected $isFragment;
    protected $isSchemeRelative;
    protected $isPathRelative;
    protected $isHostIpAddress;
    protected $schemeDelimiter;
    protected $subdomains;
    protected $domain;
    protected $tld;

    /**
     * @return mixed
     */
    public function getIsJavascript()
    {
        return $this->isJavascript;
    }

    /**
     * @param mixed $isJavascript
     * @return ReparsedUri
     */
    public function setIsJavascript($isJavascript)
    {
        $this->isJavascript = $isJavascript;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsFragment()
    {
        return $this->isFragment;
    }

    /**
     * @param mixed $isFragment
     * @return ReparsedUri
     */
    public function setIsFragment($isFragment)
    {
        $this->isFragment = $isFragment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsSchemeRelative()
    {
        return $this->isSchemeRelative;
    }

    /**
     * @param mixed $isSchemeRelative
     * @return ReparsedUri
     */
    public function setIsSchemeRelative($isSchemeRelative)
    {
        $this->isSchemeRelative = $isSchemeRelative;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPathRelative()
    {
        return $this->isPathRelative;
    }

    /**
     * @param mixed $isPathRelative
     * @return ReparsedUri
     */
    public function setIsPathRelative($isPathRelative)
    {
        $this->isPathRelative = $isPathRelative;
        return $this;
    }

    /**
     * @return bool | null
     */
    public function getIsHostIpAddress()
    {
        return $this->isHostIpAddress;
    }

    /**
     * @param bool | null $isHostIpAddress
     * @return ReparsedUri
     */
    public function setIsHostIpAddress(bool $isHostIpAddress = null)
    {
        $this->isHostIpAddress = $isHostIpAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchemeDelimiter()
    {
        return $this->schemeDelimiter;
    }

    /**
     * @param mixed $schemeDelimiter
     * @return ReparsedUri
     */
    public function setSchemeDelimiter($schemeDelimiter)
    {
        $this->schemeDelimiter = $schemeDelimiter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubdomains()
    {
        return $this->subdomains;
    }

    /**
     * @param mixed $subdomains
     * @return ReparsedUri
     */
    public function setSubdomains($subdomains)
    {
        $this->subdomains = $subdomains;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     * @return ReparsedUri
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTld()
    {
        return $this->tld;
    }

    /**
     * @param mixed $tld
     * @return ReparsedUri
     */
    public function setTld($tld)
    {
        $this->tld = $tld;
        return $this;
    }
}
