<?php

namespace Smtm\Psr\Http\Message\Uri\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Psr\Container\Factory\FactoryInterface;
use Smtm\Psr\Http\Message\Uri\CurrentUriDecorator;

class CurrentUriDecoratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $parsedUri = parse_url($_SERVER['REQUEST_URI']);

        $userInfo = $parsedUri['username'] ?? '' . (array_key_exists('password',
                $parsedUri) ? ':' . $parsedUri['password'] ?? '' : '');
        $uri      = new CurrentUriDecorator();
        $uri
            ->withUserInfo($userInfo)
            ->withScheme($parsedUri['scheme'] ?? null)
            ->withHost($parsedUri['host'] ?? null)
            ->withPort($parsedUri['port'] ?? null)
            ->withPath($parsedUri['path'] ?? null)
            ->withQuery($parsedUri['query'] ?? null)
            ->withFragment($parsedUri['fragment'] ?? null);

        return $uri;
    }
}
