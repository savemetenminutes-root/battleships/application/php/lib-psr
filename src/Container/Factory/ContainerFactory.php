<?php

namespace Smtm\Psr\Container\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Psr\Container\Container;

class ContainerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return $container;
    }

    public function create($config): ContainerInterface
    {
        $config['dependencies']['shared']['factories']['config']         = function () use ($config) {
            return $config;
        };
        $config['dependencies']['shared']['factories'][Container::class] = self::class;
        return new Container($config);
    }
}
